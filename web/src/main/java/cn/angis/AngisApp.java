package cn.angis;


import com.github.yitter.contract.IdGeneratorOptions;
import com.github.yitter.idgen.YitIdHelper;
import org.beetl.sql.core.SQLManagerBuilder;
import org.noear.solon.Solon;

/**
 * 启动器
 * @author angis.cn
 *
 */
public class AngisApp {

	public static void main(String[] args) {
		IdGeneratorOptions options = new IdGeneratorOptions((short) 9);
		options.SeqBitLength = 12;
		YitIdHelper.setIdGenerator(options);
		Solon.start(AngisApp.class, args, app -> {
			app.onEvent(SQLManagerBuilder.class, c -> {
//			启用开发或调试模式（可以打印sql）
				if (Solon.cfg().isDebugMode() || Solon.cfg().isFilesMode()) {
					c.addInterDebug();
				}
			});
		});
//		System.out.println(VaultUtils.encrypt("root"));
	}
}