cols
===
```sql
a.id,a.dept_id AS deptId,b.name AS deptName,a.username,a.nick_name AS nickName,a.type,a.email,a.phone,a.sex,a.avatar_path AS avatarPath,a.status
```

selectPage
===

```sql
select
-- @pageTag(){
#{use("cols")}
-- @}
from sys_user a INNER JOIN sys_dept b on a.dept_id=b.id where a.del_flag = #{@cn.angis.common.constant.Constant.ENABLE}
-- @if(isNotEmpty(username)){
    and a.username like #{username}
-- @}
```