cols
===
```sql
id,parent_id,title,type,permission,res_path,http_method,route_path,res_status,res_sort,menu_ext_flag,menu_cache_flag,menu_hidden_flag,menu_icon,create_date
```

selectPage
===
```sql
select
-- @pageTag(){
#{use("cols")}
-- @}
from sys_resource where 1=1
```

selectResourcesCodeByUserId
===
```sql
SELECT distinct res.permission FROM sys_user user 
    INNER JOIN sys_user_role ur on user.id=ur.user_id 
    INNER JOIN sys_role role on ur.role_id=role.id 
    INNER JOIN sys_role_res rr on rr.role_id = role.id 
    INNER JOIN sys_resource res on rr.res_id = res.id 
WHERE user.del_flag = '0' 
-- @if(isNotEmpty(userId)){
  and user.id=#{userId}
-- @}
    AND role.status = '0' AND res.permission is not null
```

listByUserId
===
```sql
SELECT distinct
    resource.id,resource.parent_id AS parentId,resource.title,resource.type,
    resource.permission,resource.route_path AS routePath,resource.vue_path AS vuePath,
    resource.component_name AS componentName,
    resource.res_status AS resStatus,resource.res_sort AS resSort,
    resource.menu_ext_flag AS menuExtFlag,resource.menu_cache_flag AS menuCacheFlag,
    resource.menu_hidden_flag AS menuHiddenFlag,resource.menu_icon AS menuIcon
FROM sys_user ui
         INNER JOIN sys_user_role ur on ui.id=ur.user_id
         INNER JOIN sys_role role on ur.role_id=role.id
         INNER JOIN sys_role_res rr on rr.role_id = role.id
         INNER JOIN sys_resource resource on rr.res_id = resource.id
WHERE ui.del_flag = '0'
-- @if(isNotEmpty(userId)){
  and ui.id=#{userId}
-- @}
  AND role.status = '0' and
    (resource.type = '1' or resource.type = '2') and resource.res_status = '0' order by resource.res_sort
```