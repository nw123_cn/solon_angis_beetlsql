cols
===
```sql
id,code,name,sort,status
```

selectPage
===

```sql
select
-- @pageTag(){
#{use("cols")}
-- @}
from sys_post where 1=1
-- @if(isNotEmpty(name)){
and name like #{name}
-- @}
```