package cn.angis.system.dtomapper;

import cn.angis.system.dto.input.DeptInput;
import cn.angis.system.dto.output.DeptOutput;
import cn.angis.system.model.Dept;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 部门表转换类
* @author angis.cn
* @Date 2023-01-05
*/
@Mapper
public interface DeptStruct {
    DeptStruct INSTANCE = Mappers.getMapper(DeptStruct.class);
    Dept toDept(DeptInput deptInput);
    DeptOutput toOutput(Dept dept);
    List<DeptOutput> toOutputList(List<Dept> deptList);
}
