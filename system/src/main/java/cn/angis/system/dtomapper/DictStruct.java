package cn.angis.system.dtomapper;

import cn.angis.system.dto.input.DictInput;
import cn.angis.system.dto.output.DictOutput;
import cn.angis.system.model.Dict;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 字典数据表转换类
* @author angis.cn
* @Date 2023-01-05
*/
@Mapper
public interface DictStruct {
    DictStruct INSTANCE = Mappers.getMapper(DictStruct.class);
    Dict toDict(DictInput dictInput);
    DictOutput toOutput(Dict dict);
    List<DictOutput> toOutputList(List<Dict> dictList);
}
