package cn.angis.system.dtomapper;

import cn.angis.system.dto.input.RoleresInput;
import cn.angis.system.dto.output.RoleresOutput;
import cn.angis.system.model.Roleres;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 角色和资源关联表转换类
* @author angis.cn
* @Date 2023-01-05
*/
@Mapper
public interface RoleresStruct {
    RoleresStruct INSTANCE = Mappers.getMapper(RoleresStruct.class);
    Roleres toRoleres(RoleresInput roleresInput);
    RoleresOutput toOutput(Roleres roleres);
    List<RoleresOutput> toOutputList(List<Roleres> roleresList);
}
