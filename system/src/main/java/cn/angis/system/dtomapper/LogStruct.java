package cn.angis.system.dtomapper;

import cn.angis.system.dto.input.LogInput;
import cn.angis.system.dto.output.LogOutput;
import cn.angis.system.model.Log;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 日志表转换类
* @author angis.cn
* @Date 2023-01-05
*/
@Mapper
public interface LogStruct {
    LogStruct INSTANCE = Mappers.getMapper(LogStruct.class);
    Log toLog(LogInput logInput);
    LogOutput toOutput(Log log);
    List<LogOutput> toOutputList(List<Log> logList);
}
