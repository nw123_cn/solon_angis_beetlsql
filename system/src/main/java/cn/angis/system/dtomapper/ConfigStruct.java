package cn.angis.system.dtomapper;

import cn.angis.system.dto.input.ConfigInput;
import cn.angis.system.dto.output.ConfigOutput;
import cn.angis.system.model.Config;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 参数配置表转换类
* @author angis.cn
* @Date 2023-01-05
*/
@Mapper
public interface ConfigStruct {
    ConfigStruct INSTANCE = Mappers.getMapper(ConfigStruct.class);
    Config toConfig(ConfigInput configInput);
    ConfigOutput toOutput(Config config);
    List<ConfigOutput> toOutputList(List<Config> configList);
}
