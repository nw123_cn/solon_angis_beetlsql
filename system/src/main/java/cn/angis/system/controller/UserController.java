package cn.angis.system.controller;

import cn.angis.common.annotation.SLog;
import cn.angis.common.api.ApiErrorCode;
import cn.angis.common.model.PageModel;
import cn.angis.common.model.PageResult;
import cn.angis.common.model.R;
import cn.angis.db.controller.BaseController;
import cn.angis.system.dto.input.PasswordInput;
import cn.angis.system.dto.input.UserInput;
import cn.angis.system.dto.output.UserOutput;
import cn.angis.system.dtomapper.UserStruct;
import cn.angis.system.model.User;
import cn.angis.system.service.UserService;
import cn.angis.system.service.UserpostService;
import cn.angis.system.service.UserroleService;
import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SmUtil;
import cn.hutool.crypto.asymmetric.SM2;
import org.noear.solon.annotation.*;
import org.noear.solon.core.handle.UploadedFile;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.Validated;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 系统用户前端控制器
 * @author angis.cn
 * @Date 2022/12/30
 */
@SLog("用户管理")
@Mapping("/system/user/")
@Controller
public class UserController extends BaseController<UserService, User> {

    @Inject
    private UserpostService userpostService;
    @Inject
    private UserroleService userroleService;
    @Inject
    private SM2 sm2;

    /**
     * 分页查询
     * @param userInput
     * @param pageModel
     * @return R<PageResult<SysUserOutput>>
     * @Date: 2022/12/30
     */
    @Get
    @SLog
    @Mapping("page")
    public R<PageResult<UserOutput>> page(UserInput userInput, PageModel pageModel) {
        User user = UserStruct.INSTANCE.toUser(userInput);
        PageResult<User> pageResult = pageListByEntity(user, pageModel);
        List<UserOutput> userOutputList = UserStruct.INSTANCE.toOutputList(pageResult.getRecords());
        return success(toPageDTO(pageResult, userOutputList));
    }

    /**
     * 保存用户
     * @param userInput
     * @return R<Boolean>
     * @Date 2022/12/31
     */
    @Post
    @SLog
    @Mapping("save")
    public R<Boolean> save(@Validated UserInput userInput) {
        User user = UserStruct.INSTANCE.toUser(userInput);
        user.setPassword(SmUtil.sm3(user.getPassword()));
        R<Boolean> result = super.save(user);
        userpostService.saveUserPost(user.getId(), userInput.getPostIds());
        userroleService.saveUserRole(user.getId(), userInput.getRoleIds());
        return result;
    }

    /**
     * 修改
     * @param userInput
     * @return R<Boolean>
     * @Date 2022/12/31
     */
    @Post
    @SLog
    @Mapping("update")
    public R<Boolean> update(@Validated UserInput userInput) {
        User user = UserStruct.INSTANCE.toUser(userInput);
        userpostService.saveUserPost(user.getId(), userInput.getPostIds());
        userroleService.saveUserRole(user.getId(), userInput.getRoleIds());
        return super.update(user);
    }

    /**
     * 修改密码
     * @param passwordInput
     * @return: R<Boolean>
     * @throws:
     * @Date: 2023/1/7
     */
    @Post
    @SLog
    @Mapping("updatePassword")
    public R<Boolean> updatePassword(@Validated PasswordInput passwordInput) {
        User user = baseService.getById(passwordInput.getUserId());
        if (!user.getPassword().equals(SmUtil.sm3(StrUtil.utf8Str(sm2.decrypt(passwordInput.getCurrentPass().getBytes()))))) {
            return R.error(ApiErrorCode.USER_UPDATE_PASS_ERROR);
        }
        passwordInput.setNewPass(StrUtil.utf8Str(sm2.decrypt(passwordInput.getNewPass().getBytes())));
        passwordInput.setConfPass(StrUtil.utf8Str(sm2.decrypt(passwordInput.getConfPass().getBytes())));
        if (!passwordInput.getNewPass().equals(passwordInput.getConfPass())) {
            return R.error(ApiErrorCode.USER_UPDATE_PASS2_ERROR);
        }
        User userUpdate = new User();
        userUpdate.setId(passwordInput.getUserId());
        userUpdate.setPassword(SmUtil.sm3(passwordInput.getNewPass()));
        return super.update(userUpdate);
    }

    /**
     * 重置密码
     * @param userId
     * @param password
     * @return: R<Boolean>
     * @throws:
     * @Date: 2023/1/7
     */
    @Post
    @SLog
    @Mapping("resetPassword")
    public R<Boolean> resetPassword(@NotBlank String userId, @NotBlank String password) {
        User user = new User();
        user.setId(userId);
        user.setPassword(SmUtil.sm3(password));
        return super.update(user);
    }

    /**
     * 根据用户名获取用户信息
     * @param username
     * @return: R<UserOutput>
     * @throws:
     * @Date: 2023/1/7
     */
    @Get
    @Mapping("getByUserName")
    public R<UserOutput> getByUsername(@NotBlank String username) {
        return success(UserStruct.INSTANCE.toOutput(baseService.getByUserName(username)));
    }

    /**
     * 获取在线用户列表
     * @return: R<List<User>>
     * @throws:
     * @Date: 2023/1/7
     */
    @Get
    @Mapping("getOnLineList")
    public R<List<UserOutput>> getOnLineUserList() {
        List<String> list = StpUtil.searchSessionId("", 0, 100, true);
        final int index = 3;
        List<String> collect = list.stream().map(s -> {
            List<String> split = StrUtil.split(s, ':');
            if (StrUtil.isNotBlank(CollUtil.get(split, index))) {
                return CollUtil.get(split, index);
            }
            return null;
        }).collect(Collectors.toList());
        return success(UserStruct.INSTANCE.toOutputList(super.listByIds(User.class, collect)));
    }

    /**
     * 删除
     * @param id
     * @return R<Boolean>
     * @Date 2022/12/31
     */
    @Post
    @SLog
    @Mapping("delete")
    public R<Boolean> delete(@NotBlank String id) {
        return super.delete(id);
    }

    /**
     * 批量删除
     * @param ids
     * @return R<Boolean>
     * @Date 2023/1/1
     */
    @Post
    @SLog
    @Mapping("deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty List<String> ids) {
        return super.deleteBatch(User.class, ids);
    }

    @Get
    @Mapping("getRoleListString")
    public R<List<String>> getRoleListString(@NotBlank String id) {
        return success(baseService.getRoleListByUserId(id));
    }

    @Get
    @Mapping("getPostListString")
    public R<List<String>> getPostListString(@NotBlank String id) {
        return success(baseService.getPostListByUserId(id));
    }

    @Post
    @Mapping("/upload")
    public R<Object> upload(UploadedFile file) {
        return upload(file, "user");
    }
}
