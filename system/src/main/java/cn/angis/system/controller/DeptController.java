package cn.angis.system.controller;

import cn.angis.common.annotation.SLog;
import cn.angis.common.model.PageModel;
import cn.angis.common.model.PageResult;
import cn.angis.common.model.R;
import cn.angis.db.controller.BaseController;
import cn.angis.system.dto.input.DeptInput;
import cn.angis.system.dto.output.DeptOutput;
import cn.angis.system.dtomapper.DeptStruct;
import cn.angis.system.model.Dept;
import cn.angis.system.service.DeptService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Post;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/**
* 部门表前端控制器
* @author angis.cn
* @Date 2023-01-05
*/
@SLog("部门管理")
@Mapping("/system/dept/")
@Controller
public class DeptController extends BaseController<DeptService, Dept> {

    /**
    * 分页查询
    * @param deptInput
    * @param pageModel
    * @return R<PageResult<DeptOutput>>
    * @Date 2023-01-05
    */
    @Get
    @SLog
    @Mapping("page")
    public R<PageResult<DeptOutput>> page(DeptInput deptInput, PageModel pageModel) {
        Dept dept = DeptStruct.INSTANCE.toDept(deptInput);
        PageResult<Dept> pageResult = pageListByEntity(dept, pageModel);
        List<DeptOutput> deptOutputList = DeptStruct.INSTANCE.toOutputList(pageResult.getRecords());
        return success(toPageDTO(pageResult, deptOutputList));
    }

    @Get
    @Mapping("tree")
    public R<List<DeptOutput>> tree() {
        List<Dept> deptTreeList = baseService.getTreeList();
        List<DeptOutput> deptOutputList = DeptStruct.INSTANCE.toOutputList(deptTreeList);
        return success(deptOutputList);
    }

    /**
    * 保存
    * @param deptInput
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @Post
    @SLog
    @Mapping("save")
    public R<Boolean> save(@Validated DeptInput deptInput) {
    Dept dept = DeptStruct.INSTANCE.toDept(deptInput);
        return super.save(dept);
    }

    /**
    * 修改
    * @param deptInput
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @Post
    @SLog
    @Mapping("update")
    public R<Boolean> update(@Validated DeptInput deptInput) {
        Dept dept = DeptStruct.INSTANCE.toDept(deptInput);
        return super.update(dept);
    }

    /**
    * 删除
    * @param id
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @Post
    @SLog
    @Mapping("delete")
    public R<Boolean> delete(@NotBlank String id) {
        return super.delete(id);
    }

    /**
    * 批量删除
    * @param ids
    * @return R<Boolean>
    * @Date 2023-01-05
    */
    @Post
    @SLog
    @Mapping("deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty List<String> ids) {
        return super.deleteBatch(Dept.class, ids);
    }
}
