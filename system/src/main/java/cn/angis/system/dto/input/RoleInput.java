package cn.angis.system.dto.input;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
* 包名称：cn.angis.system.dto.input
* 类名称：RoleInput
* 类描述：角色表
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@Data
public class RoleInput implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 角色id
	 */
	private String id;

	/**
	 * 角色名称
	 */
	private String name;

	/**
	 * 角色权限字符串
	 */
	private String code;

	/**
	 * 角色状态（0、正常；1、禁用）
	 */
	private String status;

	/**
	 * 数据范围（1、全部数据权限；2、自定数据权限；3、本部门数据权限；4、本部门及以下数据权限）
	 */
	private String dataScope;

	/**
	 * 数据权限自定义部门ids
	 */
	private List<String> deptIds;

	/**
	 * 用户id
	 */
	private String userId;

	/**
	 * 授权权限Ids
	 */
	private List<String> resIds;

}