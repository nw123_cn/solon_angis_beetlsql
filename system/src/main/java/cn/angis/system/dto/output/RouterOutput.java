package cn.angis.system.dto.output;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 包名称：cn.angis.system.dto.output
 * 类名称：RouterOutput
 * 类描述：路由地址
 * 创建人：@author angis.cn
 * 创建日期： 2023-01-05
 */
@Getter
@Setter
public class RouterOutput implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String parentId;
    /**
     * 菜单组件名称
     */
    private String name;

    /**
     * 路由地址（后端url）
     */
    private String path;

    /**
     * 前端组件路径
     */
    private String component;

    /**
     * 重定向
     */
    private String redirect;

    /**
     * 元信息
     */
    private MetaOutput meta;

    /**
     * 子级数据
     */
    private List<RouterOutput> children;

    @Getter
    @Setter
    public static class MetaOutput {

        /**
         * 标题
         */
        private String title;

        /**
         * 图标
         */
        private String icon;

        /**
         * 缓存
         */
        private Boolean isKeepAlive;

        /**
         * 菜单和目录可见（1：是；0：否）
         */
        private Boolean isHide;
    }
}


