package cn.angis.system.dto.input;

import lombok.Getter;
import lombok.Setter;
import org.noear.solon.validation.annotation.NotBlank;

import java.io.Serializable;

/**
 * 修改密码
 *
 * @author angis.cn
 * @Date 2023/1/7 17:52
 */
@Getter
@Setter
public class PasswordInput implements Serializable {
    private static final long serialVersionUID = 1L;
    @NotBlank(message = "用户id不能为空")
    private String userId;
    @NotBlank(message = "当前密码不能为空")
    private String currentPass;
    @NotBlank(message = "新密码不能为空")
    private String newPass;
    @NotBlank(message = "确认密码不能为空")
    private String confPass;
}
