package cn.angis.system.dto.output;

import lombok.Data;

import java.io.Serializable;

/**
* 包名称：cn.angis.system.dto.output
* 类名称：PostOutput
* 类描述：岗位表
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@Data
public class PostOutput implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 岗位ID
	 */
	private String id;

	/**
	 * 岗位编码
	 */
	private String code;

	/**
	 * 岗位名称
	 */
	private String name;

	/**
	 * 显示顺序
	 */
	private Long sort;

	/**
	 * 岗位状态（0、正常；1、停用）
	 */
	private String status;

}