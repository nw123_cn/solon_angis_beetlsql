package cn.angis.system.dto.output;

import lombok.Data;

import java.io.Serializable;

/**
* 包名称：cn.angis.system.dto.output
* 类名称：UserpostOutput
* 类描述：用户与岗位关联表
* 创建人：@author angis.cn
* 创建日期： 2023-01-06
*/
@Data
public class UserpostOutput implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 用户id
	 */
	private String userId;

	/**
	 * 岗位id
	 */
	private String postId;

}