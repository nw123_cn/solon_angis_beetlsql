package cn.angis.system.dto.input;

import lombok.Data;

import java.io.Serializable;

/**
* 包名称：cn.angis.system.dto.input
* 类名称：RoledeptInput
* 类描述：
* 创建人：@author angis.cn
* 创建日期： 2023-01-06
*/
@Data
public class RoledeptInput implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 角色id
	 */
	private String roleId;

	/**
	 * 部门id
	 */
	private String deptId;

}