package cn.angis.system.dto.output;

import lombok.Data;

import java.io.Serializable;

/**
* 包名称：cn.angis.system.dto.output
* 类名称：RoledeptOutput
* 类描述：
* 创建人：@author angis.cn
* 创建日期： 2023-01-06
*/
@Data
public class RoledeptOutput implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 角色id
	 */
	private String roleId;

	/**
	 * 部门id
	 */
	private String deptId;

}