package cn.angis.system.dto.input;

import lombok.Data;

import java.io.Serializable;

/**
* 包名称：cn.angis.system.dto.input
* 类名称：DictInput
* 类描述：字典数据表
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@Data
public class DictInput implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 字典id
	 */
	private String id;

	/**
	 * 父级id
	 */
	private String parentId;

	/**
	 * 字典标签
	 */
	private String label;

	/**
	 * 字典键值
	 */
	private String value;

	/**
	 * 字典类型
	 */
	private String type;

	/**
	 * 字典排序
	 */
	private Integer sort;

	/**
	 * 类型中文
	 */
	private String typeCn;

	/**
	 * 状态（0正常 1停用）
	 */
	private String status;

}