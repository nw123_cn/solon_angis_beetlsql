package cn.angis.system.model;

import cn.angis.db.model.BaseModel;
import lombok.Data;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.Table;

import java.io.Serializable;
import java.util.Date;

/**
* 包名称：cn.angis.system.model
* 类名称：Log
* 类描述：日志表
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@Table(name="sys_log")
@Data
public class Log implements Serializable {

	/**
	 * 主键id
	 */

	private String id;

	/**
	 * 操作人员
	 */

	@Column("oper_name")
	private String createBy;

	/**
	 * 请求参数
	 */

	@Column("oper_param")
	private String operParam;

	/**
	 * 请求地址
	 */

	private String url;

	/**
	 * ip地址
	 */

	private String ip;

	/**
	 * 业务模块名称
	 */

	@Column("business_name")
	private String businessName;

	/**
	 * 方法名
	 */

	private String method;

	/**
	 * 返回结果
	 */

	private String result;

	/**
	 * 操作状态（0正常 1异常）
	 */

	@Column("log_status")
	private String logStatus;

	/**
	 * 错误信息
	 */

	private String error;

	@Column("create_date")
	private Date createDate;

}