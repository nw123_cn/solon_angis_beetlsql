package cn.angis.system.model;

import lombok.Data;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.Table;

import java.io.Serializable;

/**
* 包名称：cn.angis.system.model
* 类名称：Roledept
* 类描述：
* 创建人：@author angis.cn
* 创建日期： 2023-01-06
*/
@Table(name="sys_role_dept")
@Data
public class Roledept implements Serializable {

	private static final long sserialVersionUID = 1L;

	public Roledept(){}
	public Roledept(String roleId,String deptId) {
		this.deptId = deptId;
		this.roleId = roleId;
	}
	/**
	 * 角色id
	 */

	@Column("role_id")
	private String roleId;

	/**
	 * 部门id
	 */

	@Column("dept_id")
	private String deptId;

}