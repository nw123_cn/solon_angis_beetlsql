package cn.angis.system.model;

import cn.angis.db.model.BaseModel;
import lombok.Data;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.LogicDelete;
import org.beetl.sql.annotation.entity.Table;

/**
* 包名称：cn.angis.system.model
* 类名称：Post
* 类描述：岗位表
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@Table(name="sys_post")
@Data
public class Post extends BaseModel {

	/**
	 * 岗位ID
	 */
	private String id;

	/**
	 * 岗位编码
	 */
	private String code;

	/**
	 * 岗位名称
	 */
	private String name;

	/**
	 * 显示顺序
	 */
	private Long sort;

	/**
	 * 岗位状态（0、正常；1、停用）
	 */
	private String status;

	@LogicDelete(1)
	@Column("del_flag")
	private String delFlag;

}