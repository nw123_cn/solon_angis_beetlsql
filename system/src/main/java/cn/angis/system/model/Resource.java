package cn.angis.system.model;

import cn.angis.common.model.TreeModel;
import lombok.Data;
import lombok.experimental.Accessors;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.Table;

import java.io.Serializable;
import java.util.List;

/**
* 包名称：cn.angis.system.model
* 类名称：Resource
* 类描述：资源表
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@Table(name="sys_resource")
@Data
@Accessors(chain = true)
public class Resource implements TreeModel<Resource>, Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * 主键id
	 */

	private String id;

	/**
	 * 父级id
	 */

	@Column("parent_id")
	private String parentId;

	/**
	 * 标题（目录名称、菜单名称、按钮名称）
	 */

	private String title;

	/**
	 * 类型（1、目录；2、菜单；3、按钮）
	 */

	private String type;

	/**
	 * 权限标识（菜单和按钮）
	 */

	private String permission;

	/**
	 * 前台组件路径
	 */

	@Column("vue_path")
	private String vuePath;

	@Column("component_name")
	private String componentName;

	/**
	 * 请求方式（GET或者POST等等）
	 */

	@Column("http_method")
	private String httpMethod;

	/**
	 * 路由地址（后台url）
	 */

	@Column("route_path")
	private String routePath;

	/**
	 * 状态（0、正常；1、禁用）
	 */

	@Column("res_status")
	private String resStatus;

	/**
	 * 排序
	 */

	@Column("res_sort")
	private Long resSort;

	/**
	 * 外链菜单（1：是；0：否）
	 */

	@Column("menu_ext_flag")
	private String menuExtFlag;

	/**
	 * 菜单缓存（1：是；0：否）
	 */

	@Column("menu_cache_flag")
	private String menuCacheFlag;

	/**
	 * 菜单和目录可见（1：是；0：否）
	 */

	@Column("menu_hidden_flag")
	private String menuHiddenFlag;

	/**
	 * 菜单图标
	 */

	@Column("menu_icon")
	private String menuIcon;

	private List<Resource> children;

}