package cn.angis.system.model;

import lombok.Data;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.Table;

import java.io.Serializable;

/**
* 包名称：cn.angis.system.model
* 类名称：Roleres
* 类描述：角色和资源关联表
* 创建人：@author angis.cn
* 创建日期： 2023-01-05
*/
@Table(name="sys_role_res")
@Data
public class Roleres implements Serializable {

	private static final long sserialVersionUID = 1L;

	public Roleres(){}
	public Roleres(String roleId, String resId) {
		this.resId = resId;
		this.roleId = roleId;
	}
	/**
	 * 角色id
	 */

	@Column("role_id")
	private String roleId;

	/**
	 * 资源id
	 */

	@Column("res_id")
	private String resId;

}