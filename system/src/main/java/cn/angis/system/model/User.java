package cn.angis.system.model;

import cn.angis.db.model.BaseModel;
import lombok.Data;
import org.beetl.sql.annotation.entity.*;
import org.noear.solon.validation.annotation.NotNull;

import java.util.List;

/**
 * 包名称：cn.angis.system.model
 * 类名称：User
 * 类描述：用户表
 * 创建人：@author angis.cn
 * 创建日期： 2023-02-21
 */
@Table(name="sys_user")
@Data
public class User extends BaseModel {

    /**
     * 用户id
     */

    private String id;

    /**
     * 部门id
     */

    @Column("dept_id")
    private String deptId;

    /**
     * 用户名
     */

    private String username;

    /**
     * 密码
     */

    private String password;

    /**
     * 用户昵称
     */

    @Column("nick_name")
    private String nickName;

    /**
     * 用户类型（0、管理员；1、普通用户）
     */

    private String type;

    /**
     * 用户邮箱
     */

    private String email;

    /**
     * 手机号码
     */

    private String phone;

    /**
     * 性别（0、男；1、女）
     */

    private String sex;

    /**
     * 头像
     */

    @Column("avatar_path")
    private String avatarPath;

    /**
     * 帐号状态（0、正常；1、禁用）
     */

    private String status;

    /**
     * 删除标识
     */

    @LogicDelete(1)
    @Column("del_flag")
    private String delFlag;

    /**
     * 备注
     */

    private String remark;
    private List<String> postIds;
    private List<String> roleIds;
    private String deptName;

}