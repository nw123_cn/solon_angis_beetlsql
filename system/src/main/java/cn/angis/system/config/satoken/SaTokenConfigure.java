package cn.angis.system.config.satoken;


import cn.angis.common.api.ApiErrorCode;
import cn.angis.common.model.R;
import cn.angis.system.service.ResourceService;
import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.dao.SaTokenDao;
import cn.dev33.satoken.exception.DisableServiceException;
import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.exception.NotPermissionException;
import cn.dev33.satoken.exception.NotRoleException;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.solon.dao.SaTokenDaoOfRedis;
import cn.dev33.satoken.solon.integration.SaTokenInterceptor;
import cn.dev33.satoken.stp.StpUtil;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;


/**
 * [Sa-Token 权限认证] 配置类 
 * @author kong
 * @author noear
 * @ignore
 */
@Configuration
public class SaTokenConfigure {

	@Inject
	private ResourceService resourceService;

	@Bean
	public SaTokenDao saTokenDaoInit(@Inject("${sa-token-dao.redis}") SaTokenDaoOfRedis saTokenDao) {
		return saTokenDao;
	}

	/**
	 * 注册 [sa-token全局过滤器]
	 *
	 * @ignore
	 */
	@Bean
	public SaTokenInterceptor tokenPathFilter() {
		return new AngisSaTokenPathInterceptor(resourceService)

				// 指定 [拦截路由] 与 [放行路由]
				.addInclude("/**").addExclude("/favicon.ico", "/login")

				// 认证函数: 每次请求执行
				.setAuth(r -> {
				})

				// 异常处理函数：每次认证函数发生异常时执行此函数
				.setError(e -> {
					String message = "";
					if (e instanceof NotLoginException) {
						switch (((NotLoginException) e).getType()) {
							case NotLoginException.NOT_TOKEN:
								message = NotLoginException.NOT_TOKEN_MESSAGE;
								break;
							case NotLoginException.INVALID_TOKEN:
								message = NotLoginException.INVALID_TOKEN_MESSAGE;
								break;
							case NotLoginException.TOKEN_TIMEOUT:
								message = NotLoginException.TOKEN_TIMEOUT_MESSAGE;
								break;
							case NotLoginException.BE_REPLACED:
								message = NotLoginException.BE_REPLACED_MESSAGE;
								break;
							case NotLoginException.KICK_OUT:
								message = NotLoginException.KICK_OUT_MESSAGE;
								break;
							default:
								message = NotLoginException.DEFAULT_MESSAGE;
								break;
						}
						return R.restResult(ApiErrorCode.TOKEN_ERROR, message);
					} else if (e instanceof NotRoleException) {
						return R.restResult(ApiErrorCode.USER_NOROLE_LOCKED_ERROR, e.getMessage());
					} else if (e instanceof NotPermissionException) {
						return R.restResult(ApiErrorCode.USER_NOPERMISSION_LOCKED_ERROR, e.getMessage());
					} else if (e instanceof DisableServiceException) {
						return R.restResult(ApiErrorCode.USER_DISABLE, ((DisableServiceException) e).getDisableTime() + "秒后解封");
					} else {
						return R.error(e.getMessage());
					}
				})

				// 前置函数：在每次认证函数之前执行
				.setBeforeAuth(r -> {
					SaRouter.match("/**", StpUtil::checkLogin);
					// ---------- 设置一些安全响应头 ----------
					SaHolder.getResponse()
							// 服务器名称
							.setServer("sa-server")
							// 是否可以在iframe显示视图： DENY=不可以 | SAMEORIGIN=同域下可以 | ALLOW-FROM uri=指定域名下可以
							.setHeader("X-Frame-Options", "SAMEORIGIN")
							// 是否启用浏览器默认XSS防护： 0=禁用 | 1=启用 | 1; mode=block 启用, 并在检查到XSS攻击时，停止渲染页面
							.setHeader("X-Frame-Options", "1; mode=block")
							// 禁用浏览器内容嗅探
							.setHeader("X-Content-Type-Options", "nosniff")
					;
				});
	}
}
