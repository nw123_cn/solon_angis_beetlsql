package cn.angis.system.config.satoken;

import cn.angis.system.service.ResourceService;
import cn.angis.system.service.RoleService;
import cn.dev33.satoken.stp.StpInterface;
import org.noear.solon.annotation.Component;
import org.noear.solon.annotation.Inject;

import java.util.List;

/**
 * 自定义权限验证接口扩展
 * @ignore
 */
@Component    // 打开此注解，保证此类被 solon 扫描，即可完成 sa-token 的自定义权限验证扩展
public class StpInterfaceImpl implements StpInterface {

	@Inject
	private RoleService roleService;
	@Inject
	private ResourceService resourceService;
	/**
	 * 返回一个账号所拥有的权限码集合
	 * @ignore
	 */
	@Override
	public List<String> getPermissionList(Object loginId, String loginType) {
		return resourceService.selectResourcesCodeByUserId((String) loginId);
	}

	/**
	 * 返回一个账号所拥有的角色标识集合
	 * @ignore
	 */
	@Override
	public List<String> getRoleList(Object loginId, String loginType) {
		return roleService.selectRoleKeyByUserId((String) loginId);
	}

}
