package cn.angis.system.service;

import cn.angis.db.service.BaseService;
import cn.angis.system.mapper.RoleMapper;
import cn.angis.system.model.Role;
import org.beetl.sql.core.SQLReady;
import org.beetl.sql.core.SqlId;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.ProxyComponent;

import java.util.List;
import java.util.Map;

/**
* 角色表服务
* @author angis.cn
* @Date 2023-01-05
*/
@ProxyComponent
public class RoleService extends BaseService<RoleMapper, Role> {

    @Inject
    private RoledeptService roledeptService;

    /**
     * 根据用户Id获取角色key列表
     * @param userId
     * @return: List<String>
     * @throws: 
     * @Date: 2023/1/8     
     */
    public List<String> selectRoleKeyByUserId(String userId) {
        return baseMapper.getSQLManager().select(SqlId.of("role.selectRoleKeyByUserId"), String.class, Map.of("userId", userId));
    }

    /**
     * 根据用户Id获取角色列表
     * @param userId
     * @return: List<Role>
     * @throws: 
     * @Date: 2023/1/8     
     */
    public List<Role> getRoleListByUserId(String userId) {
        return baseMapper.getSQLManager().select(SqlId.of("role.getRoleListByUserId"), Role.class, Map.of("userId", userId));
    }

    public List<String> getDataScopeDeptIdsByRoleId(String roleId) {
        String sql = "SELECT DISTINCT a.dept_id FROM sys_role_dept a WHERE a.role_id=?";
        return baseMapper.getSQLManager().execute(new SQLReady(sql, roleId), String.class);
    }
}