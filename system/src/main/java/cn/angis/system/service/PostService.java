package cn.angis.system.service;

import cn.angis.db.service.BaseService;
import cn.angis.system.mapper.PostMapper;
import cn.angis.system.model.Post;
import org.noear.solon.annotation.ProxyComponent;

/**
* 岗位表服务
* @author angis.cn
* @Date 2023-01-05
*/
@ProxyComponent
public class PostService extends BaseService<PostMapper, Post> {
}