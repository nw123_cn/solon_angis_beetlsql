package cn.angis.system.service;

import cn.angis.db.service.BaseService;
import cn.angis.system.mapper.RoledeptMapper;
import cn.angis.system.model.Roledept;
import cn.hutool.core.collection.CollUtil;
import org.beetl.sql.core.SQLReady;
import org.noear.solon.annotation.ProxyComponent;
import org.noear.solon.data.annotation.Tran;

import java.util.ArrayList;
import java.util.List;

/**
* 服务
* @author angis.cn
* @Date 2023-01-06
*/
@ProxyComponent
public class RoledeptService extends BaseService<RoledeptMapper, Roledept> {
    @Tran
    public int deleteByRoleId(String roleId) {
        return baseMapper.getSQLManager().executeUpdate(new SQLReady("delete from sys_role_dept where role_id=?", roleId));
    }

    /**
     * 保存角色部门关联表
     * @param roleId
     * @param deptIds
     * @return: void
     * @throws: 
     * @Date: 2023/1/8     
     */
    @Tran
    public void saveRoleDepts(String roleId, List<String> deptIds) {
        if (CollUtil.isNotEmpty(deptIds)) {
            this.deleteByRoleId(roleId);
            List<Roledept> roledeptList = new ArrayList<>();
            deptIds.forEach(deptId -> roledeptList.add(new Roledept(roleId, deptId)));
            this.saveBatch(roledeptList);
        }
    }
}