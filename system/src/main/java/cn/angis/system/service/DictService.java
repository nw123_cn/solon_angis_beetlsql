package cn.angis.system.service;

import cn.angis.common.constant.Constant;
import cn.angis.db.service.BaseService;
import cn.angis.system.dto.output.DictOutput;
import cn.angis.system.dtomapper.DictStruct;
import cn.angis.system.mapper.DictMapper;
import cn.angis.system.model.Dict;
import cn.hutool.core.util.StrUtil;
import org.noear.solon.annotation.ProxyComponent;
import org.noear.solon.data.annotation.Cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
* 字典数据表服务
* @author angis.cn
* @Date 2023-01-05
*/
@ProxyComponent
public class DictService extends BaseService<DictMapper, Dict> {
    public List<Dict> listByType(String type) {
        return baseMapper.getSQLManager().lambdaQuery(Dict.class)
                .andEq(Dict::getType, type)
                .andEq(Dict::getStatus, Constant.ENABLE)
                .orderBy(Dict::getStatus)
                .select(Dict::getLabel, Dict::getValue);
    }

    @Cache(key=Constant.CACHE_DICT_MAP, tags = Constant.CACHE_DICT_MAP)
    private Map<String, List<Dict>> getMapDict() {
        Map<String, List<Dict>> dictMap = new HashMap<>();
        for (Dict dict : listAll()){
            List<Dict> dictList = dictMap.get(dict.getType());
            if (dictList != null){
                dictList.add(dict);
            }else{
                dictMap.put(dict.getType(), new ArrayList<Dict>(List.of(dict)));
            }
        }
        return dictMap;
    }

    public Map<String, List<DictOutput>> getMapByTypes(String types) {
        Map<String, List<DictOutput>> result = new HashMap<>();
        if (StrUtil.isNotBlank(types)) {
            String[] typeArr = types.split(",");
            for (String type : typeArr) {
                result.put(type.replace("-", ""), getListByType(type));
            }
        }
        return result;
    }

    private List<DictOutput> getListByType(String type) {
        return DictStruct.INSTANCE.toOutputList(getMapDict().get(type));
    }

    private List<Dict> listAll() {
        return baseMapper.getSQLManager().lambdaQuery(Dict.class)
                .andEq(Dict::getStatus, Constant.ENABLE)
                .select(Dict::getValue, Dict::getLabel, Dict::getType);
    }
}