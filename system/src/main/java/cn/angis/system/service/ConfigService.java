package cn.angis.system.service;

import cn.angis.db.service.BaseService;
import cn.angis.system.mapper.ConfigMapper;
import cn.angis.system.model.Config;
import org.noear.solon.annotation.ProxyComponent;

/**
* 参数配置表服务
* @author angis.cn
* @Date 2023-01-05
*/
@ProxyComponent
public class ConfigService extends BaseService<ConfigMapper, Config> {
}