package cn.angis.system.service;

import cn.angis.db.service.BaseService;
import cn.angis.system.mapper.LogMapper;
import cn.angis.system.model.Log;
import org.noear.solon.annotation.ProxyComponent;

/**
* 日志表服务
* @author angis.cn
* @Date 2023-01-05
*/
@ProxyComponent
public class LogService extends BaseService<LogMapper, Log> {
}