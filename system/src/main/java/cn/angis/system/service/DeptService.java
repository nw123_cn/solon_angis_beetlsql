package cn.angis.system.service;

import cn.angis.common.util.TreeUtil;
import cn.angis.db.service.BaseService;
import cn.angis.system.mapper.DeptMapper;
import cn.angis.system.model.Dept;
import org.noear.solon.annotation.ProxyComponent;

import java.util.List;

/**
* 部门表服务
* @author angis.cn
* @Date 2023-01-05
*/
@ProxyComponent
public class DeptService extends BaseService<DeptMapper, Dept> {
    public List<Dept> getTreeList() {
        List<Dept> deptsList = baseMapper.getSQLManager().lambdaQuery(Dept.class)
                .select();
        return TreeUtil.listToTree(deptsList);
    }
}