package cn.angis.generate.builder;

import cn.angis.generate.util.ExtractFieldUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.github.yitter.idgen.YitIdHelper;
import org.beetl.core.Template;
import org.beetl.sql.core.engine.template.Beetl;
import org.beetl.sql.core.engine.template.BeetlTemplateEngine;
import org.beetl.sql.gen.BaseProject;
import org.beetl.sql.gen.Entity;
import org.beetl.sql.gen.SourceConfig;
import org.beetl.sql.gen.simple.BaseTemplateSourceBuilder;

import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MenuSQLBuilder extends BaseTemplateSourceBuilder {
    public static String mapperTemplate = "menuSql.html";
    private String module;

    public MenuSQLBuilder() {
        super("db/menu");
    }

    public MenuSQLBuilder(String module) {
        super("db/menu");
        this.module = module;
    }

    public void generate(BaseProject project, SourceConfig config, Entity entity) {
        Beetl beetl = ((BeetlTemplateEngine) config.getSqlManager().getSqlTemplateEngine()).getBeetl();

        List<String> idList = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            idList.add(String.valueOf(YitIdHelper.nextId()));
        }

        Template template = groupTemplate.getTemplate(mapperTemplate);
        template.binding("module", this.module);
        template.binding("className", entity.getName());
        template.binding("tableName", entity.getTableName());
        template.binding("idList", idList);
        template.binding("currentDate", DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        template.binding("comment", StrUtil.isNotEmpty(entity.getComment()) ? ExtractFieldUtil.call(entity.getComment()) : entity.getName());

        String mdFileName = "insert_menu_" + entity.getTableName() + ".sql";
        Writer writer = project.getWriterByName(this.name, mdFileName);
        template.renderTo(writer);
    }
}
