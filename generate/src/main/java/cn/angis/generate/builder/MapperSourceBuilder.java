package cn.angis.generate.builder;

import cn.angis.generate.util.LowerFirstFnUtil;
import org.beetl.core.Template;
import org.beetl.sql.gen.BaseProject;
import org.beetl.sql.gen.Entity;
import org.beetl.sql.gen.SourceConfig;
import org.beetl.sql.gen.simple.BaseTemplateSourceBuilder;

import java.io.Writer;
import java.util.Arrays;

public class MapperSourceBuilder extends BaseTemplateSourceBuilder {
    public static String mapperPath = "mapper.html";
    private String module;

    public MapperSourceBuilder() {
        super("mapper");
    }

    public MapperSourceBuilder(String module) {
        super("mapper");
        this.module = module;
    }

    public void generate(BaseProject project, SourceConfig config, Entity entity) {
        Template template = groupTemplate.getTemplate(mapperPath);
        // 注册方法
        groupTemplate.registerFunction("lowerFirst", new LowerFirstFnUtil());

        template.binding("module", this.module);
        template.binding("className", entity.getName());
        template.binding("cols", "${cols}");
        template.binding("attrs", entity.getList());
        template.binding("tableName", entity.getTableName());
        template.binding("package", project.getBasePackage(this.name));
        template.binding("entityClass", entity.getName());
        String entityPkg = project.getBasePackage("entity");
        String mapperHead = entityPkg + ".*";
        template.binding("imports", Arrays.asList(mapperHead));
        Writer writer = project.getWriterByName(this.name, entity.getName() + "Mapper.java");
        template.renderTo(writer);
    }
}
