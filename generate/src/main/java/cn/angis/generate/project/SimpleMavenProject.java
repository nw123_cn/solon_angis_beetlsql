package cn.angis.generate.project;

import org.beetl.sql.gen.BaseProject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class SimpleMavenProject extends BaseProject {
    private String basePackage = null;


    public SimpleMavenProject(String basePackage) {
        this.basePackage = basePackage;
    }

    public SimpleMavenProject() {
        this.basePackage = "cn.angis";
    }

    public Writer getWriterByName(String sourceBuilderName, String targetName) {
        String home = this.root;
        FileWriter writer = null;
        String src;
        String output;
        if (sourceBuilderName.indexOf("sql")>-1 || sourceBuilderName.equals("db/menu") || sourceBuilderName.equals("doc") || sourceBuilderName.indexOf("vue")>-1) {
            src = this.root + File.separator + "generate/src/test/resources/" + sourceBuilderName;
            output = src + File.separator + targetName;

            try {
                this.checkFile(output);
                writer = new FileWriter(new File(output));
            } catch (IOException var11) {
                throw new IllegalArgumentException(output);
            }
        } else {
            src = this.root + File.separator + "generate/src/test/java";
            output = this.getBasePackage(sourceBuilderName);
            String subPath = output.replace('.', File.separatorChar);
            output = src + File.separator + subPath + File.separator + targetName;

            try {
                this.checkFile(output);
                writer = new FileWriter(new File(output));
            } catch (IOException var10) {
                throw new IllegalArgumentException(output, var10);
            }
        }

        return writer;
    }

    public String getBasePackage(String sourceBuilerName) {
        return this.basePackage + "." + sourceBuilerName;
    }

    protected void checkFile(String filePath) throws IOException {
        File file = new File(filePath);
        File fileParent = file.getParentFile();
        if (!fileParent.exists()) {
            fileParent.mkdirs();
        }

        if (!file.exists()) {
            file.createNewFile();
        }

    }

    public void setBasePackage(String basePackage) {
        this.basePackage = basePackage;
    }

}
