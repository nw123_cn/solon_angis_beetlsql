package cn.angis.generate.util;

import cn.angis.generate.model.GenerModel;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.setting.dialect.Props;

public class GenUtils {
    private static final Props DB_PROPS;

    static {
        DB_PROPS = new Props("db.properties", CharsetUtil.UTF_8);
    }

    public static GenerModel getGenerByProps() {
        GenerModel generModel = new GenerModel();
        generModel.setPack(DB_PROPS.getStr("db.pack"));
        generModel.setDbUrl(DB_PROPS.getStr("db.url"));
        generModel.setDriverName(DB_PROPS.getStr("db.driverName"));
        generModel.setUsername(DB_PROPS.getStr("db.username"));
        generModel.setPassword(DB_PROPS.getStr("db.password"));
        generModel.setTableName(DB_PROPS.getStr("table.tableName"));
        generModel.setPrefix(DB_PROPS.getStr("table.prefix"));
        generModel.setParent(DB_PROPS.getStr("package.parent"));
        generModel.setModule(DB_PROPS.getStr("package.module"));
        return generModel;
    }

}
