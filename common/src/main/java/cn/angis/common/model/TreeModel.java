package cn.angis.common.model;

import java.util.List;

/**
 * 树形
 *
 * @author angis.cn
 * @date 2023/1/1 16:59
 */
public interface TreeModel<T> {

    /**
     * 主键id
     * @return
     */
    String getId();

    /**
     * 父id
     * @return
     */
    String getParentId();

    /**
     * 设置子数据集
     * @param children
     * @return
     */
    T setChildren(List<T> children);
}
