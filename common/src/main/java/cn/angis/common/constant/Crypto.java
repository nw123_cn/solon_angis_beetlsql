package cn.angis.common.constant;

import cn.hutool.core.util.HexUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.SmUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.SM2;
import cn.hutool.crypto.symmetric.SymmetricAlgorithm;

/**
 * 包名称：cn.angis.common.constant
 * 类名称：Crypto
 * 类描述：
 * 创建人：@author angis.cn
 * 创建日期： 2023/1/5 17:59
 */
public class Crypto {
    public static final String PUBLICKEY = "3059301306072a8648ce3d020106082a811ccf5501822d034200042992a9c134e53541b5514074a9cfb25a20d218d4e50bd01923ab690c2f3539a9f45a28ead88f5e99776cc3af3800be61399866ed16087a9ec987845ccda70daa";
    public static final String PRIVATEKEY = "308193020100301306072a8648ce3d020106082a811ccf5501822d047930770201010420dd66e56994e3d07bbd6ed274057131d36bb32e988ef514967050e10bbfe60494a00a06082a811ccf5501822da144034200042992a9c134e53541b5514074a9cfb25a20d218d4e50bd01923ab690c2f3539a9f45a28ead88f5e99776cc3af3800be61399866ed16087a9ec987845ccda70daa";
    public static final String SM4KEY = "0p9dgezis6j1i0h9la6nx0nc4ls5z49d";

    public static void main(String[] args) throws Exception {

//        String sm4Key = RandomUtil.randomString(RandomUtil.BASE_CHAR_NUMBER, 32);
//        System.out.println("sm4Key:"+sm4Key);
//
//        KeyPair pair = SecureUtil.generateKeyPair("SM2");
//
//        // 生成私钥
//        String privateKey = HexUtil.encodeHexStr(pair.getPrivate().getEncoded());
//        System.out.println("私钥:"+privateKey);
//        // 生成公钥
//        String publicKey = HexUtil.encodeHexStr(pair.getPublic().getEncoded());
//        System.out.println("公钥:"+publicKey);
//
//        // 生成私钥D
//        String privateKeyD = HexUtil.encodeHexStr(BCUtil.encodeECPrivateKey(pair.getPrivate())); // ((BCECPrivateKey) privateKey).getD().toByteArray();
//        System.out.println("私钥D:"+privateKeyD);
//
//        // 生成公钥Q，以q值做为js端的加密公钥
//        String publicKeyQ = HexUtil.encodeHexStr(((BCECPublicKey) pair.getPublic()).getQ().getEncoded(false));
//        System.out.println("公钥Q:"+publicKeyQ);

//        SM2 sm2 = SmUtil.sm2(Crypto.PRIVATEKEY, Crypto.PUBLICKEY);
//        System.out.println(HexUtil.encodeHexStr(sm2.encrypt("123456", KeyType.PublicKey)));
//        String decryptStr = StrUtil.utf8Str(sm2.decrypt(HexUtil.encodeHexStr("044c97aadb38c56197d88deb409406a869507ab3baeb5d8232552c948fb1ea8d3f56ab7be9f4c83269e1dbbc70d227f5213d705ca1050d2d9a220d5b6cfb4abe8fe2550a755d6ad584477ffaa3b567441b807c44c1dc24322802d224730318a8302bf163bec7e4"), KeyType.PrivateKey));
//        System.out.printf("decryptStr:%s \n",decryptStr);


        String text ="123456";
        // 随机生成秘钥
//        KeyPair pair = SecureUtil.generateKeyPair("SM2");
//        byte[] privateKey = pair.getPrivate().getEncoded();
//        byte[] publicKey = pair.getPublic().getEncoded();
//         /*私钥
//          完整:308193020100301306072a8648ce3d020106082a811ccf5501822d047930770201010420730895115e28d7cdbc5be77e0a2c39f690e04217b7218f7c36f27e293b7d1e51a00a06082a811ccf5501822da1440342000498024e7a2ad38f79223394aab9a30fd30be81c6c5efd307d520ff5f53d9d4bee2f7e62c843ae2c0ff448dc6e56297fda6154d5110ea246e4b692c2d3bda96949
//          拆解:
//          308193020100301306072a8648ce3d0201
//          06082a811ccf5501822d
//          047930770201010420
//          730895115e28d7cdbc5be77e0a2c39f690e04217b7218f7c36f27e293b7d1e51  //即为js私钥
//          a00a
//          06082a811ccf5501822d
//          a144
//          034200
//          0498024e7a2ad38f79223394aab9a30fd30be81c6c5efd307d520ff5f53d9d4bee2f7e62c843ae2c0ff448dc6e56297fda6154d5110ea246e4b692c2d3bda96949
//         */
//        System.out.printf("privateKey:%s \n",HexUtil.encodeHexStr(privateKey));
//          /* 公钥
//           完整:3059301306072a8648ce3d020106082a811ccf5501822d0342000498024e7a2ad38f79223394aab9a30fd30be81c6c5efd307d520ff5f53d9d4bee2f7e62c843ae2c0ff448dc6e56297fda6154d5110ea246e4b692c2d3bda96949
//           拆解:
//           3059301306072a8648ce3d0201
//           06082a811ccf5501822d
//           034200
//    js公钥: 0498024e7a2ad38f79223394aab9a30fd30be81c6c5efd307d520ff5f53d9d4bee2f7e62c843ae2c0ff448dc6e56297fda6154d5110ea246e4b692c2d3bda96949
//         */
//        System.out.printf("publicKey:%s \n",HexUtil.encodeHexStr(publicKey));


        SM2 sm2 = SmUtil.sm2(Crypto.PRIVATEKEY, Crypto.PUBLICKEY);

        // 公钥加密
        String encryptStr = HexUtil.encodeHexStr(sm2.encrypt(text, KeyType.PublicKey));
        // 04BA601FE569074DDFC34E4AE2925E0B62B129F0A16F8A39FA4D1BF9FE274F556695694C787CEE218F3A9575FBCEA5115019E8B469FE07444894B25A78E2914EB5B5A42BEE510B84F64D2785DD351A200B8739A0286C04B7CC314D4C57A0998250221B22D50A4285B32F6C8132799A0A5B4117EBA4E122818C
        // 去掉前置04并且全小写 js可解密内容：
        // ba601fe569074ddfc34e4ae2925e0b62b129f0a16f8a39fa4d1bf9fe274f556695694c787cee218f3a9575fbcea5115019e8b469fe07444894b25a78e2914eb5b5a42bee510b84f64d2785dd351a200b8739a0286c04b7cc314d4c57a0998250221b22d50a4285b32f6c8132799a0a5b4117eba4e122818c
        System.out.printf("encryptStr:%s \n",encryptStr);
        // 私钥解密
        String decryptStr = StrUtil.utf8Str(sm2.decrypt(encryptStr, KeyType.PrivateKey));
        System.out.printf("decryptStr:%s \n",decryptStr);

        // js加密内容 java解密 已做过处理
//        SM2 sm3 = SmUtil.sm2(ByteUtils.fromHexString(Crypto.PRIVATEKEY),ByteUtils.fromHexString(Crypto.PUBLICKEY));
        String jsEncryptStr ="047f0902f831e57e687b4af53667a5d579e5e95c326fe84ba33c281e2ef25816a3815b5de3cca3982cf39ef2af8eaea34bc5aa93f0a752b1db6f0cb0418138cbef0b7d4ead612e4ada36a7066352242c2ae010475a4dc24d8b49f25bd8e7e32740d2453509fc47";
        String jsDecryptStr = StrUtil.utf8Str(sm2.decryptFromBcd(jsEncryptStr, KeyType.PrivateKey));

        System.out.printf("jsDecryptStr:%s \n",jsDecryptStr);
        //AES key
        byte[] aesKey = SecureUtil.generateKey(SymmetricAlgorithm.AES.getValue()).getEncoded();
        System.out.printf("aes key:%s", HexUtil.encodeHexStr(aesKey));
    }
}
