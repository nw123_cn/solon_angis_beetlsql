package cn.angis.common.constant;

import org.noear.solon.Solon;

import java.util.HashMap;
import java.util.Map;

/**
 * 包名称：cn.angis.common.constant
 * 类名称：Constant
 * 类描述：全局常量
 * 创建人：@author angis.cn
 * 创建日期： 2023/1/5 20:25
 */
public class Constant {
    public static final String CACHE_PRE = "angis:";
    public static final String SESSION_USER_KEY = CACHE_PRE + "user";
    public static final String SESSION_USER_NAME = CACHE_PRE + "loginname";
    public static final String LOGIN_ERROR_NUMBER = CACHE_PRE + "login_error";
    public static final String ENABLE = "0";
    public static final String DISABLE = "1"; //目录
    public static final String MENU = "2"; //菜单
    public static final String BUTTON = "3"; //按钮
    public static final String COMMON_PARENT_ID = "0";
    public static final String REDIS_CAPTCHA_KEY = CACHE_PRE + "user:captcha:";
    public static final String ROUTER_LAYOUT = "Layout";
    public static final String ROUTER_PARENT_VIEW = "ParentView";

    public static final String CACHE_DICT_MAP = CACHE_PRE + "allmapdict";
    public static final long USER_LOCK_TIME = 6 * 60 *60;
    public static final Map<String, String> OPERAMAP = new HashMap<String, String>() {
        {
            put("page", "分页查询");
            put("save", "新增");
            put("update", "修改");
            put("del", "删除");
        }
    };
}
