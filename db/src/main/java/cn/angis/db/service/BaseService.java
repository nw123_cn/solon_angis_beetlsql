package cn.angis.db.service;

import cn.angis.common.constant.Constant;
import cn.angis.db.mapper.AngisMapper;
import cn.dev33.satoken.stp.StpUtil;
import com.github.yitter.idgen.YitIdHelper;
import org.beetl.sql.annotation.entity.Table;
import org.beetl.sql.core.SqlId;
import org.beetl.sql.core.page.PageRequest;
import org.beetl.sql.core.page.PageResult;
import org.beetl.sql.solon.annotation.Db;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 包名称：cn.angis.db.model
 * 类名称：BaseService
 * 类描述：服务基类
 * 创建人：@author angis.cn
 * 创建日期： 2022/11/29 20:38
 */
public abstract class BaseService<M extends AngisMapper<T>, T> {
    @Db
    protected M baseMapper;

    /**
     * 根据对象id，查询详细信息
     *
     * @param id
     * @return Entity
     */
    public T getById(Serializable id) {
        return baseMapper.single(id);
    }

    /**
     * 添加
     *
     * @param entity
     * @return
     */
    public void save(T entity) {
        setInsertParam(entity);
        baseMapper.insertTemplate(entity);
    }

    /**
     * 批量添加
     *
     * @param list<T>
     * @return
     */
    public void saveBatch(List<T> list) {
        for (int i=0; i<list.size(); i++) {
            setInsertParam(list.get(i));
        }
        baseMapper.insertBatch(list);
    }

    /**
     * 修改
     *
     * @param entity
     * @return int
     */
    public int update(T entity) {
        setUpdateParam(entity);
        return baseMapper.updateTemplateById(entity);
    }

    /**
     * 批量修改
     *
     * @param list
     * @return int
     */
    public int[] updateBatch(List<T> list) {
        for (int i=0; i<list.size(); i++) {
            setUpdateParam(list.get(i));
        }
        return baseMapper.getSQLManager().updateByIdBatch(list);
    }

    /**
     * 删除
     *
     * @param id
     * @return int
     */
    public int delete(Serializable id) {
        return baseMapper.deleteById(id);
    }

    /**
     * 逻辑批量删除
     *
     * @param cls
     * @param list
     * @return int[]
     */
    public int[] deleteBatchLogic(Class<?> cls, List<String> list) {
        Table table = cls.getAnnotation(Table.class);
        String tabName = table.name();
        SqlId sqlId = baseMapper.getSQLManager().getSqlIdFactory().buildSql("update "+tabName+" set del_flag=1 where id=? ");
        return baseMapper.getSQLManager().updateBatch(sqlId, list);
    }

    /**
     * 批量删除
     *
     * @param cls
     * @param list
     * @return int[]
     */
    public int[] deleteBatch(Class<?> cls, List<String> list) {
        Table table = cls.getAnnotation(Table.class);
        String tabName = table.name();
        SqlId sqlId = baseMapper.getSQLManager().getSqlIdFactory().buildSql("delete from "+tabName+" where id=? ");
        return baseMapper.getSQLManager().updateBatch(sqlId, list);
    }

    /**
     * 分页查询 根据分页参数查询
     *
     * @param pageRequest    分页参数
     * @param entity 实体查询条件
     * @return
     */
    public PageResult<T> page(T entity, PageRequest pageRequest) {
        return baseMapper.selectPage(pageRequest, entity);
    }

    /**
     * 修改
     *
     * @return int
     */
    public List<T> all(Class<?> cls) {
        Table table = cls.getAnnotation(Table.class);
        String tabName = table.name();
        String sql = "select * from "+ tabName +" where del_flag=0";
        return baseMapper.execute(sql, null);
    }

    /**
     * 根据用户id集合查询实体列表
     * @param cls
 * @param ids
     * @return: List<T>
     * @throws: 
     * @Date: 2023/1/7     
     */
    public List<T> listByIds(Class<?> cls, List<String> ids) {
        Table table =  cls.getAnnotation(Table.class);
        String tabName = table.name();
        String sql = "select * from "+ tabName +" where del_flag=0 and id in ( #{join(ids)} )";
        Map paras = new HashMap();
        paras.put("ids", ids);
        return baseMapper.execute(sql,paras);
    }

    private void setInsertParam(T entity) {
        Class cls = entity.getClass();
        Field f = null;
        try {
            f = hasField(cls, "id");
            if (null != f) {
                f.setAccessible(true);
                f.set(entity, String.valueOf(YitIdHelper.nextId()));
            }
            f = hasField(cls, "createDate");
            if (null != f) {
                f.setAccessible(true);
                f.set(entity, new Date());
            }
            f = hasField(cls, "createBy");
            if (null != f) {
                f.setAccessible(true);
                if (null == StpUtil.getSessionByLoginId(Constant.SESSION_USER_NAME, false)) {
                    f.set(entity, Thread.currentThread().getName());
                } else {
                    f.set(entity, StpUtil.getSession().get(Constant.SESSION_USER_NAME));
                }
            }
        } catch (Exception e) {
        }
    }

    private void setUpdateParam(T entity) {
        Class cls = entity.getClass();
        Field f = null;
        try {
            f = hasField(cls, "updateDate");
            if (null != f) {
                f.setAccessible(true);
                f.set(entity, new Date());
            }
            f = hasField(cls, "updateBy");
            if (null != f) {
                f.setAccessible(true);
                f.set(entity, StpUtil.getSession().get(Constant.SESSION_USER_NAME));
            }
        } catch (Exception e) {
        }
    }

    private Field hasField(Class c, String fieldName){
        Field[] fields = c.getDeclaredFields();
        for (Field f : fields) {
            if (fieldName.equals(f.getName())) {
                return f;
            }
        }
        return null;
    }
}
