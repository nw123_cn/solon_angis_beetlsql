package cn.angis.db.model;

import lombok.Getter;
import lombok.Setter;
import org.beetl.sql.annotation.entity.Column;
import org.beetl.sql.annotation.entity.LogicDelete;

import java.io.Serializable;
import java.util.Date;

/**
 * 包名称：cn.angis.db.model
 * 类名称：BaseService
 * 类描述：基础实体
 * 创建人：@author angis.cn
 * 创建日期： 2022/11/28 10:33
 */
@Getter
@Setter
public class BaseModel implements Serializable {
    private static final long sserialVersionUID = 1L;

    /**
     * 创建者
     */
    @Column("create_by")
    private String createBy;

    /**
     * 创建时间
     */
    @Column("create_date")
    private Date createDate;

    /**
     * 更新者
     */
    @Column("update_by")
    private String updateBy;

    /**
     * 更新时间
     */
    @Column("update_date")
    private Date updateDate;

}
