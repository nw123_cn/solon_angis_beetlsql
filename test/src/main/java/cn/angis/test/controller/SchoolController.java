package cn.angis.test.controller;

import cn.angis.common.annotation.SLog;
import cn.angis.common.model.PageModel;
import cn.angis.common.model.PageResult;
import cn.angis.common.model.R;
import cn.angis.db.controller.BaseController;
import cn.angis.test.dto.input.SchoolInput;
import cn.angis.test.dto.output.SchoolOutput;
import cn.angis.test.dtomapper.SchoolStruct;
import cn.angis.test.model.School;
import cn.angis.test.service.SchoolService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.annotation.Post;
import org.noear.solon.validation.annotation.NotBlank;
import org.noear.solon.validation.annotation.NotEmpty;
import org.noear.solon.validation.annotation.Validated;

import java.util.List;

/**
* 学校前端控制器
* @author angis.cn
* @Date 2023-03-25
*/
@SLog("学校")
@Mapping("/test/school/")
@Controller
public class SchoolController extends BaseController<SchoolService, School> {

    /**
    * 分页查询
    * @param schoolInput
    * @param pageModel
    * @return R<PageResult<SchoolOutput>>
    * @Date 2023-03-25
    */
    @SLog
    @Get
    @Mapping("page")
    public R<PageResult<SchoolOutput>> page(SchoolInput schoolInput, PageModel pageModel) {
        School school = SchoolStruct.INSTANCE.toSchool(schoolInput);
        PageResult<School> pageResult = pageListByEntity(school, pageModel);
        List<SchoolOutput> schoolOutputList = SchoolStruct.INSTANCE.toOutputList(pageResult.getRecords());
        return success(toPageDTO(pageResult, schoolOutputList));
    }

    /**
    * 保存
    * @param schoolInput
    * @return R<Boolean>
    * @Date 2023-03-25
    */
    @SLog
    @Post
    @Mapping("save")
    public R<Boolean> save(@Validated SchoolInput schoolInput) {
    School school = SchoolStruct.INSTANCE.toSchool(schoolInput);
        return super.save(school);
    }

    /**
    * 修改
    * @param schoolInput
    * @return R<Boolean>
    * @Date 2023-03-25
    */
    @SLog
    @Post
    @Mapping("update")
    public R<Boolean> update(@Validated SchoolInput schoolInput) {
        School school = SchoolStruct.INSTANCE.toSchool(schoolInput);
        return super.update(school);
    }

    /**
    * 删除
    * @param id
    * @return R<Boolean>
    * @Date 2023-03-25
    */
    @SLog
    @Post
    @Mapping("delete")
    public R<Boolean> delete(@NotBlank String id) {
        return super.delete(id);
    }

    /**
    * 批量删除
    * @param ids
    * @return R<Boolean>
    * @Date 2023-03-25
    */
    @SLog
    @Post
    @Mapping("deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty List<String> ids) {
        return super.deleteBatch(School.class, ids);
    }
}
