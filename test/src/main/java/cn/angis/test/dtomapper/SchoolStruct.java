package cn.angis.test.dtomapper;

import cn.angis.test.dto.input.SchoolInput;
import cn.angis.test.dto.output.SchoolOutput;
import cn.angis.test.model.School;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
* 学校转换类
* @author angis.cn
* @Date 2023-03-25
*/
@Mapper
public interface SchoolStruct {
    SchoolStruct INSTANCE = Mappers.getMapper(SchoolStruct.class);
    School toSchool(SchoolInput schoolInput);
    SchoolOutput toOutput(School school);
    List<SchoolOutput> toOutputList(List<School> schoolList);
}
