package cn.angis.test.service;

import cn.angis.db.service.BaseService;
import cn.angis.test.mapper.SchoolMapper;
import cn.angis.test.model.School;
import org.noear.solon.aspect.annotation.Service;

/**
* 学校服务
* @author angis.cn
* @Date 2023-03-25
*/
@Service
public class SchoolService extends BaseService<SchoolMapper, School> {
}