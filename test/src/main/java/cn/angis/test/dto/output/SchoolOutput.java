package cn.angis.test.dto.output;

import lombok.Data;

import java.io.Serializable;

/**
* 包名称：cn.angis.test.dto.output
* 类名称：SchoolOutput
* 类描述：学校
* 创建人：@author angis.cn
* 创建日期： 2023-03-25
*/
@Data
public class SchoolOutput implements Serializable {
	private static final long serialVersionUID = 1L;


	/**
	 * 用户id
	 */
	private Long id;

	/**
	 * 部门id
	 */
	private Long deptId;

	/**
	 * 用户名
	 */
	private String username;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * 用户昵称
	 */
	private String nickName;

	/**
	 * 用户类型（0、管理员；1、普通用户）
	 */
	private String type;

	/**
	 * 用户邮箱
	 */
	private String email;

	/**
	 * 手机号码
	 */
	private String phone;

	/**
	 * 性别（0、男；1、女）
	 */
	private String sex;

	/**
	 * 头像
	 */
	private String avatarPath;

	/**
	 * 帐号状态（0、正常；1、禁用）
	 */
	private String status;

	/**
	 * 删除标识
	 */
	private String delFlag;

	/**
	 * 备注
	 */
	private String remark;

}