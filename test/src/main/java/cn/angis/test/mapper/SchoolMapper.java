package cn.angis.test.mapper;

import cn.angis.db.mapper.AngisMapper;
import cn.angis.test.model.School;
import org.beetl.sql.mapper.annotation.SqlResource;

/**
* 包名称：cn.angis.test.mapper
* 类名称：SchoolMapper
* 类描述：
* 创建人：@author angis.cn
* 创建日期： 2023-03-25
*/
@SqlResource("school")
public interface SchoolMapper extends AngisMapper<School> {

}