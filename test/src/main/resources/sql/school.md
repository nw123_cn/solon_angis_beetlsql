cols
===
```sql
id,dept_id AS deptId,username,password,nick_name AS nickName,type,email,phone,sex,avatar_path AS avatarPath,status,create_by AS createBy,create_date AS createDate,update_by AS updateBy,update_date AS updateDate,del_flag AS delFlag,remark
```

selectPage
===

```sql
select
-- @pageTag(){
#{use("cols")}
-- @}
from test_school where 1=1
-- @if(isNotEmpty(id)){
and id=#{id}
-- @}
-- @if(isNotEmpty(deptId)){
and dept_id=#{deptId}
-- @}
-- @if(isNotEmpty(username)){
and username=#{username}
-- @}
-- @if(isNotEmpty(password)){
and password=#{password}
-- @}
-- @if(isNotEmpty(nickName)){
and nick_name=#{nickName}
-- @}
-- @if(isNotEmpty(type)){
and type=#{type}
-- @}
-- @if(isNotEmpty(email)){
and email=#{email}
-- @}
-- @if(isNotEmpty(phone)){
and phone=#{phone}
-- @}
-- @if(isNotEmpty(sex)){
and sex=#{sex}
-- @}
-- @if(isNotEmpty(avatarPath)){
and avatar_path=#{avatarPath}
-- @}
-- @if(isNotEmpty(status)){
and status=#{status}
-- @}
-- @if(isNotEmpty(createBy)){
and create_by=#{createBy}
-- @}
-- @if(isNotEmpty(createDate)){
and create_date=#{createDate}
-- @}
-- @if(isNotEmpty(updateBy)){
and update_by=#{updateBy}
-- @}
-- @if(isNotEmpty(updateDate)){
and update_date=#{updateDate}
-- @}
-- @if(isNotEmpty(delFlag)){
and del_flag=#{delFlag}
-- @}
-- @if(isNotEmpty(remark)){
and remark=#{remark}
-- @}
```